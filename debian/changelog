r-cran-uwot (0.2.3-1) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * Team upload.
  * Removed skip for autodep8 test on i386 as it was malformed, but
    fortunately it was ignored and not needed.

  [ Charles Plessy ]
  * New upstream version
  * Standards-Version: 4.7.2 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Fri, 28 Feb 2025 17:48:05 +0900

r-cran-uwot (0.2.2-2) unstable; urgency=medium

  * Team upload.
  * Autopkgtests: skip on i386.

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 29 Jan 2025 14:45:29 +0100

r-cran-uwot (0.2.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)

 -- Charles Plessy <plessy@debian.org>  Mon, 02 Sep 2024 13:22:36 +0900

r-cran-uwot (0.1.16-2) unstable; urgency=medium

  * Rebuild with r-graphics-engine set by r-base
    Closes: #1039674

 -- Andreas Tille <tille@debian.org>  Thu, 06 Jul 2023 10:08:42 +0200

r-cran-uwot (0.1.16-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 30 Jun 2023 22:03:46 +0200

r-cran-uwot (0.1.15-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Patch for test not needed any more

 -- Andreas Tille <tille@debian.org>  Thu, 29 Jun 2023 15:02:45 +0200

r-cran-uwot (0.1.14-2) unstable; urgency=medium

  * Team Upload.
  * Skip a couple of tests that choke on !amd64

 -- Nilesh Patra <nilesh@debian.org>  Sat, 17 Sep 2022 23:45:59 +0530

r-cran-uwot (0.1.14-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 22 Aug 2022 14:51:16 +0200

r-cran-uwot (0.1.11-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on r-cran-rcppannoy.

 -- Andreas Tille <tille@debian.org>  Fri, 10 Dec 2021 11:39:56 +0100

r-cran-uwot (0.1.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 19 Dec 2020 16:30:43 +0530

r-cran-uwot (0.1.9-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Set upstream metadata fields: Bug-Submit, Repository.

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 23 Nov 2020 20:54:10 +0530

r-cran-uwot (0.1.8-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Sun, 22 Mar 2020 09:37:15 +0100

r-cran-uwot (0.1.5-2) unstable; urgency=medium

  * Merged own packaging with the one by Andreas.
  * TODO: Added dependency on libtbb-dev, does not FTBFS without it
    but states not to find it - which it still does when added,
    which remains to be investigated.

 -- Steffen Moeller <moeller@debian.org>  Tue, 31 Dec 2019 03:00:05 +0100

r-cran-uwot (0.1.5-1) unstable; urgency=medium

  * New upstream version of what I now know not to have
    been a new package (closes: #947796)
  * Added dependency on libpcg-cpp-dev for pcg_random.hpp, FTBFS without

 -- Steffen Moeller <moeller@debian.org>  Mon, 30 Dec 2019 22:46:18 +0100

r-cran-uwot (0.1.4-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.4.1
  * Trim trailing whitespace.
  * Set upstream metadata fields: Archive, Bug-Database, Repository.

 -- Andreas Tille <tille@debian.org>  Mon, 11 Nov 2019 21:19:40 +0100

r-cran-uwot (0.1.3-1) unstable; urgency=medium

  * Initial release (closes: #940083)

 -- Andreas Tille <tille@debian.org>  Thu, 12 Sep 2019 09:24:43 +0200
